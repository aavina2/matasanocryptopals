#ifndef _HEXTO64_H_
#define _HEXTO64_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

// OpenSSL includes
/*#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>
#include <openssl/evp.h>
#include <openssl/aes.h>
*/

char hexToInt(char hex);

/**
 * Pretty Hex to raw Byte
 */
unsigned char* pHexToBytes(char *hex, int *rawSize);

/**
 * Raw Bytes to Pretty 64
 */
char* rawBytesToP64(unsigned char *rawBytes, int size);

/**
 * Pretty base-64 to raw Bytes
 */
unsigned char* p64ToRaw(char *str, int *rawsize);

/**
 * Raw Bytes to Pretty Hex
 */
char* rawBytesToPHex(unsigned char *rawBytes, int size);

/**
 * Pretty hex to Pretty base-64
 */
char* pHexToP64(char *hex);

#endif // _HEXTO64_H_
