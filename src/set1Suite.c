#include "set1Suite.h"

const char* CH4FILENAME = "./res/4.txt";
const char* CH6FILENAME = "./res/6.txt";
const char* CH7FILENAME = "./res/7.txt";
const char* CH8FILENAME = "./res/8.txt";

void challenge1() {

    char *STRARG = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";

    char *ANS =     "SSdtIGtpbGxpbmcgeW91ciBicmFpbiBsaWtlIGEgcG9pc29ub3VzIG11c2hyb29t";
    char *str = malloc(strlen(STRARG)+1);

    printf("\nRunning Challenge 1:\n---------------------\n");

    strcpy(str, STRARG);

    char *result = pHexToP64(str);
    printf("Result is:\t%s\n", result);
    printf("ANS:\t\t%s\n", ANS);
    if(strcmp(result, ANS) == 0)
        printf("Result was correct!\n");
    else
        printf("Wrong result!\n");

    free(str);
    free(result);
}

void challenge2() {
    char *arg =     "1c0111001f010100061a024b53535009181c";
    char *against = "686974207468652062756c6c277320657965";
    char *answer =  "746865206b696420646f6e277420706c6179";

    printf("\nRunning Challenge 2:\n---------------------\n");
    
    char *result = fixedXOR(arg, against);

    printf("Result is: \t%s\n", result);
    printf("Answer:\t\t%s\n", answer);
    if(strcmp(result, answer) == 0)
        printf("Result was correct!\n");
    else
        printf("Wrong result!\n");

    free(result);
}

void challenge3() {
    char *arg = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736";
    int bestScore = 0;
    int currentScore = 0;
    char bestCharacter;
    char bestString[100];
    char *currentString;
    int i;

    printf("\nRunning Challenge 3:\n---------------------\n");

    for(i=0; i < 127; i++) {
        currentString = encryptDecrypt(i, arg);
        currentScore = scoreLetterFrequency(currentString);

        if(currentScore > bestScore) {
            bestScore = currentScore;
            bestCharacter = i;
            strcpy(bestString, currentString);
        }
        free(currentString);
    }
    printf("Best score was %d for character %c\nBest String: %s\n", bestScore, bestCharacter, bestString);
}

void challenge4() {
    FILE *fp; // The file descriptor
    int bestScore = 0;
    int currentScore = 0;
    int i;
    int done = 0;
    char line[100];
    char bestString[100];
    char *currentStringDec;

    printf("\nRunning Challenge 4:\n---------------------\n");

    fp = fopen(CH4FILENAME, "r");
    if(fp == NULL) {
        printf("Couldn't open file!\n");
        return;
    }
    while(readLine(fp, line, 100, &done)) {
        if(done)
            break;
        for(i=0; i < 127; i++) {
            currentStringDec = encryptDecrypt(i, line);
            currentScore = scoreLetterFrequency(currentStringDec);

            if(currentScore > bestScore) {
                bestScore = currentScore;
                strcpy(bestString, currentStringDec);
            }
            free(currentStringDec);
        }
    }

    // Close file
    if(fp != NULL)
        fclose(fp);

    printf("Best Score: %d\n", bestScore);
    printf("String Decrypted: %s", bestString);
}

void challenge5() {
    char *str = "Burning 'em, if you ain't quick and nimble\nI go crazy when I hear a cymbal";
    char *ans = "0b3637272a2b2e63622c2e69692a23693a2a3c6324202d623d63343c2a26226324272765272a282b2f20430a652e2c652a3124333a653e2b2027630c692b20283165286326302e27282f";
    char *key = "ICE";
    char *encryptedText;
    printf("\nRunning Challenge 5:\n---------------------\n");

    encryptedText = repeatKeyXOR(str, key);

    printf("Encrypted Text:\t%s\n", encryptedText);
    printf("Answer text:\t%s\n", ans);
    if(strcmp(encryptedText, ans) == 0)
        printf("Result is correct!\n");
    else
        printf("Result is incorrect!\n");

    // Cleanup
    free(encryptedText);
}

void challenge6() {
    FILE *fp;
    unsigned int filesize;
    unsigned char *rawdata;
    char *filedata;
    char *plaintext;
    char *bestPlain = 0;
    unsigned char *key;
    char bestStrKey[41];
    int rawSize;
    int i;
    int score = 0;
    int bestScore = 0;
    int bestKey;

    printf("\nRunning Challenge 6:\n---------------------\n");

    fp = fopen(CH6FILENAME, "r");
    if(fp == NULL) {
        printf("Couldn't open file!\n");
        return;
    }

    // Find filesize
    fseek(fp, 0L, SEEK_END);
    filesize = ftell(fp) + 1;
    filedata = malloc(filesize);
    filedata[filesize-1] = '\0';
    fseek(fp, 0L, SEEK_SET);

    // Read entire file 
    readFile(fp, filedata, filesize);

    // Close the file
    if(fp != NULL)
        fclose(fp);

    // Convert filedata (pretty hex) to raw bytes
    rawdata = p64ToRaw(filedata, &rawSize);

    // Brute force the cipher
    for(i=2; i <= 40; i++) {
        key = malloc(i+1);
        plaintext = blockSolver(rawdata, rawSize, i, key, 0);
        score = scoreLetterFrequency(plaintext);
        if(score > bestScore) {
            if(bestPlain != 0)
                free(bestPlain);
            bestPlain = plaintext;
            bestScore = score;
            memcpy(bestStrKey, key, i+1);
            free(key);
            bestKey = i;
        }
        else {
            free(plaintext);
            free(key);
        }
    }

    printf("Best plaintext:\t%s\n", bestPlain);
    printf("Best score:\t%d\n", bestScore);
    printf("Best key:\t%d\n", bestKey);

    free(bestPlain);
    free(rawdata);
    free(filedata);
}

void challenge7() {
    FILE *fp;
    int placeholder = 0;
    int filesize = 0;
    int rawsize;
    char *data;
    char *plaintext;
    unsigned char *rawdata;
    unsigned char aes_key[] = "YELLOW SUBMARINE";
    AES_KEY dec_key;

    printf("\nRunning Challenge 7:\n---------------------\n");

    AES_set_decrypt_key(aes_key, strlen((char*)aes_key)*8, &dec_key);

    fp = fopen(CH7FILENAME, "r");
    if(fp == NULL) {
        printf("Couldn't open file!\n");
        return;
    }

    // Find filesize
    fseek(fp, 0L, SEEK_END);
    filesize = ftell(fp) + 1;
    data = malloc(filesize);
    data[filesize-1] = '\0';
    fseek(fp, 0L, SEEK_SET);

    // Read entire file 
    readFile(fp, data, filesize);

    if(fp != NULL)
        fclose(fp);

    // Convert filedata (pretty hex) to raw bytes
    rawdata = p64ToRaw(data, &rawsize);
    plaintext = malloc(rawsize+1);
    plaintext[rawsize] = '\0';

    while(placeholder < rawsize) {
        AES_ecb_encrypt(rawdata + placeholder,
                (unsigned char*)(plaintext + placeholder), &dec_key, AES_DECRYPT); 
        placeholder += strlen((char*)aes_key);
    }

    printf("Plaintext: \n%s\n", plaintext);

    free(plaintext);
    free(rawdata);
    free(data);
}

void challenge8() {
    FILE *fp;
    char filebuf[1000];
    char bestStr[1000];
    int done = 0;
    int curDups = 0;
    int bestDups = 0;
    int linenum = 0;
    int bestline = 0;


    printf("\nRunning Challenge 8:\n---------------------\n");

    // Open file
    fp = fopen(CH8FILENAME, "r");
    if(fp == NULL) {
        printf("Couldn't open file!\n");
        return;
    }

    while(readLine(fp, filebuf, 1000, &done)) {
        if(done)
            break;

        // Split into 32 byte blocks
        curDups = blockECBDetector(filebuf);

        if(curDups > bestDups) {
            bestline = linenum;
            bestDups = curDups;
            strcpy(bestStr, filebuf);
        }

        linenum++;
    }

    printf("Best Line: %d\tWith duplicates: %d\n", bestline, bestDups);
    printf("Encrypted String: %s\n", bestStr);

    // Close file
    if(fp != NULL)
        fclose(fp);
}

void set1Suite() {
    challenge1();
    challenge2();
    challenge3();
    challenge4();
    challenge5();
    challenge6();
    challenge7();
    challenge8();
}
