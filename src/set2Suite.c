#include "set2Suite.h"


void challenge9() {
    char *str = "YELLOW SUBMARINE";
    char *stuff;
    unsigned char *paddedStr;
    
    paddedStr = pkcs7Pad(str, '\x04', 20);

    stuff = malloc(21);
    stuff[20] = '\0';
    memcpy(stuff, paddedStr, 20);

    printf("Original str:\t%s\n", str);
    printf("PaddedStr:\t%s\n", stuff);

    free(stuff);
    free(paddedStr);
}

void set2Suite() {
    challenge9();
}
