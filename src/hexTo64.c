#include "hexTo64.h"

char base16table[16] =
    {'0','1','2','3','4',
     '5','6','7','8','9', 
     'a','b','c','d','e','f'}; 

char base64table[64] =
    {'A','B','C','D','E','F','G','H','I','J',
    'K','L','M','N','O','P','Q','R','S','T',
    'U','V','W','X','Y','Z','a','b','c','d',
    'e','f','g','h','i','j','k','l','m','n',
    'o','p','q','r','s','t','u','v','w','x',
    'y','z','0','1','2','3','4','5','6','7',
    '8','9','+','/'};

char hexToInt(char hex) {
    if(hex >= '0' && hex <= '9')
        hex -= '0';
    else if(hex >= 'a' && hex <= 'f')
        hex = (hex - 'a') + 10;

    return hex;
}

char p64ToInt(char p64) {
    if(p64 >= 'A' && p64 <= 'Z')
        p64 -= 'A';
    else if(p64 >= 'a' && p64 <= 'z')
        p64 = (p64 - 'a') + 26;
    else if(p64 >= '0' && p64 <= '9')
        p64 = (p64 - '0') + 52;
    else if(p64 == '+')
        p64 = 62;
    else if(p64 == '/')
        p64 = 63;

    return p64;
}

/**
 * Pretty Hex to raw Byte
 */
unsigned char* pHexToBytes(char *hex, int *rawSize) {
    int i,j;
    int len = strlen(hex);
    int newlen = ((len/2) + (len%2));
    unsigned char curByte = 0;
    unsigned char *rawBytes = malloc(newlen);
    j = newlen-1;
    *rawSize = newlen;

    for(i=len-1; i >= 0; i-=2) {
        curByte = hexToInt(hex[i]);
        if(i-1 >= 0)
            curByte |= (hexToInt(hex[i-1]) << 4);
        rawBytes[j--] = curByte;
    }

    return rawBytes;
}

/**
 * Raw Bytes to Pretty 64
 */
char* rawBytesToP64(unsigned char *rawBytes, int size) {
    // plen: 8 for bits per byte, 6 for bits per b64 char,
    // size%2 for any bits left over, 1 for null char
    int plen = ((size*8)/6) + (size % 2) + 1;
    int i, j = size-1;
    int carry = 0; // Indicates how many bits are being carried
    unsigned char *pretty = malloc(plen);
    pretty[plen-1] = '\0';

    for(i = plen-2; i >= 0; i--) {
        if(carry == 0) {
            pretty[i] = base64table[(rawBytes[j] & 0x3f)];
            if(i-1 >= 0) 
                pretty[i-1] = (rawBytes[j] & 0xc0) >> 6;

            carry = 2;
        }
        else if(carry == 2) {
            pretty[i] |= ((rawBytes[j] & 0x0f) << carry);
            pretty[i] = base64table[pretty[i]];
            pretty[i-1] = (rawBytes[j] & 0xf0) >> 4;
            if(i-1 < 0)
                pretty[i-1] = base64table[pretty[i-1]];

            carry = 4;
        }
        else if(carry == 4) {
            pretty[i] |= (rawBytes[j] & 0x03) << carry;
            pretty[i] = base64table[pretty[i]];
            if(i-1 >= 0)
                pretty[--i] = base64table[(rawBytes[j] & 0xfc) >> 2];
            carry = 0;
        }
        j--;
    }

    return (char*)pretty;
}

// Converts p64 to raw bytes 
// Returns a char * that is NOT null-terminated
unsigned char* p64ToRaw(char *str, int *rawsize) {
    int i,j;
    int len = strlen(str);
    unsigned char *rawBytes;
    char *p0, *p1, *p2, *p3; // the 4 base64 to convert into ASCII 
    j=0;

    *rawsize = ((len/4)*3);
    rawBytes = malloc(*rawsize);

    // Turn all the p64 chars into numerics
    for(i=0; i < len; i++)
        str[i] = p64ToInt(str[i]);

    // Group base64 into 4's and convert to ASCII
    for(i=0; i < len; i+=4) {
        p0 = str + i;
        p1 = p0+1;
        p2 = p1+1;
        p3 = p2+1;

        *p0 = (*p0 << 2) | (*p1 >> 4);
        *p1 = ((*p1 & 0x0f) << 4) | ((*p2 >> 2) & 0x0f);
        *p2 = ((*p2 & 0x03) << 6) | *p3;

        memcpy(rawBytes + j, str + i, 3);
        j += 3;
    }

    return rawBytes;
}

char* rawBytesToPHex(unsigned char *rawBytes, int size) {
    int newlen = size*2+1;
    char *pretty = malloc(newlen);
    pretty[newlen-1] = '\0';
    int i,j=0;

    for(i=0; i < newlen-1; i++) {
        pretty[i++] = base16table[(rawBytes[j] & 0xf0) >> 4];
        pretty[i] = base16table[(rawBytes[j] & 0x0f)];
        j++;
    }

    return pretty;
}

char* pHexToP64(char *hex) {
    int rawSize = 0;
    unsigned char *rawBytes = pHexToBytes(hex, &rawSize);
    char *pretty64;
    //char *tester = malloc(rawSize+1);

    // Print for fun
    /*
    memcpy(tester, rawBytes, rawSize);
    tester[rawSize] = '\0';
    free(tester);
    */

    pretty64 = rawBytesToP64(rawBytes, rawSize);

    free(rawBytes);
    return pretty64;
}
