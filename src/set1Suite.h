#ifndef _SET1SUITE_H_
#define _SET1SUITE_H_

#include "auxFunctions.h"

void challenge1();
void challenge2();
void challenge3();
void challenge4();
void challenge5();
void challenge6();
void challenge7();
void challenge8();
void set1Suite();

#endif // _SET1SUITE_H_
