#include "auxFunctions.h"

/**
 * Accepts 2 hex encoded strings, converts them to raw byte buffers,
 * XORs them, then returns a newly allocated char*
 * to a pretty Hex format string
 */
char* fixedXOR(char *arg1, char *arg2) {
    char *pretty;
    unsigned char *rawBuf1;
    unsigned char *rawBuf2;
    int rawlen1;
    int rawlen2;
    int i;

    rawBuf1 = pHexToBytes(arg1, &rawlen1);
    rawBuf2 = pHexToBytes(arg2, &rawlen2);

    if(rawlen1 == rawlen2) {
        for(i=0; i < rawlen1; i++)
            rawBuf1[i] ^= rawBuf2[i];
    }

    pretty = rawBytesToPHex(rawBuf1, rawlen1);
    free(rawBuf1);
    free(rawBuf2);

    return pretty;
}

/**
 * Takes a string and counts how many vowels are in it.
 * The return value is the total amount of vowels
 * found within the string (a,e,i,o,u)
 **/
int scoreLetterFrequency(char *str) {
    int score = 0;
    int len = strlen(str);
    int i;

    for(i=0; i < len; i++) {
        switch(str[i]) {
            case 'a':
                score++;
                break;
            case 'e':
                score++;
                break;
            case 'i':
                score++;
                break;
            case 'o':
                score++;
                break;
            case 'u':
                score++;
                break;
            case 'A':
                score++;
                break;
            case 'E':
                score++;
                break;
            case 'I':
                score++;
                break;
            case 'O':
                score++;
                break;
            case 'U':
                score++;
                break;
            case ' ':
                score++;
                break;
            default:
                break;
        }
    }

    return score;
}

/**
 * key:     the char to enc/dec with 
 * str:     the string to enc/dec 
 **/
char* encryptDecrypt(char key, char *str) {
    // This will hold a char array all set to 'character'
    int len = strlen(str);
    int newlen = ((len/2) + (len%2) + 1);
    char *result = malloc(newlen);
    char currentByte = 0;
    int i, j = newlen-1;
    result[j] = '\0';
    j--;

    // Transform input string into hexArray 
    for(i=len-1; i >= 0; i -= 2) {
        currentByte = 0;
        currentByte = hexToInt(str[i]); // Least Sig bits
        if(i-1 >= 0)
            currentByte |= (hexToInt(str[i-1]) << 4); // OR Most sig bits

        // XOR byte with key 
        currentByte ^= key;

        // Split byte into 4 bit groups again and store
        result[j--] = currentByte;
    }

    return result;
}

unsigned char* encryptDecryptRaw(unsigned char key, unsigned char *str, int len) {
    unsigned char *result = malloc(len);
    int i, j = len-1;

    for(i=0; i < len; i++)
        result[j--] = str[i] ^ key;

    return result;
}

char* encryptDecryptRawECB(unsigned char key, unsigned char *str, int len) {
    unsigned char *extendedStr;
    unsigned char *result = malloc(17);
    unsigned char aes_key[17];
    int i;
    AES_KEY dec_key;

    // Extend str to 16 bytes so that aes cipher can work
    extendedStr = malloc(17);
    memcpy(extendedStr, str, len);
    if(len < 16) {
        for(i=len; i < 16; i++)
            extendedStr[i] = 1; 
        extendedStr[16] = '\0';
    }

    result[len] = '\0';

    for(i=0; i < 16; i++)
        aes_key[i] = key;
    aes_key[16] = '\0';

    AES_set_decrypt_key(aes_key, 16*8, &dec_key);

    for(i=0; i <= len; i+=16){
        AES_ecb_encrypt(extendedStr + i,
                result + i, &dec_key, AES_DECRYPT); 
    }
    result[len] = '\0';

    return (char*)result;
}

/**
 * FP is the file descriptor
 * buffer is where the bytes are read to
 * bufsize is how big the read buffer is
 * Returns how many bytes were read
 */
int readLine(FILE *fp, char *buffer, int bufsize, int *done) {
    int bytesRead = 0;
    char curChar;

    while(1) {
        curChar = fgetc(fp);
        if(curChar == EOF) {
            *done = 1;
            break;
        }
        else if(curChar == '\n') {
            buffer[bytesRead++] = '\0';
            break;
        }
        else {
            buffer[bytesRead++] = curChar;
            if(bytesRead+1 == bufsize) {
                buffer[bytesRead] = '\0';
                break;
            }
        }

    }
    return bytesRead;
}

/**
 * Reads an entire file to buffer.
 * Breaks when bufsize has been reached OR EOF reached
 * Returns how many bytes have been read
 */
int readFile(FILE *fp, char *buffer, int bufsize) {
    int bytesRead = 0;
    int curChar;

    while(1) {
        curChar = fgetc(fp);
        if(curChar == EOF) {
            break;
        }
        else {
            if(curChar != '\n') {
                buffer[bytesRead++] = curChar;
                if(bytesRead+1 == bufsize) {
                    buffer[bytesRead] = '\0';
                    break;
                }
            }
        }
    }
    return bytesRead;
}

char findXORKey(unsigned char *data, int datasize, int ecb) {
    unsigned char i;
    int currentScore = 0;
    int bestScore = 0;
    char bestkey;
    char *decoded;

    for(i=1; i < 127; i++) {
        if(ecb) {
            decoded = encryptDecryptRawECB(i, data, datasize);
        }
        else
            decoded = (char*)encryptDecryptRaw(i, data, datasize);

        currentScore = scoreLetterFrequency(decoded);

        if(currentScore > bestScore) {
            bestScore = currentScore;
            bestkey = i;
        }

        free(decoded);

    }

    return bestkey;
}

/**
 * Applies the key to the data via XOR, cycling through its characters
 * data: String, null-terminated 
 * key:  Expected to be null-terminated
 * return: A newly allocated string pointer to raw data 
 */
char* repeatKeyXOR(char *data, char *key) {
    char *prettyEnc;
    unsigned char *encText = malloc(strlen(data)+1);
    int i;

    memcpy(encText, data, strlen(data));
    encText[strlen(data)] = '\0';
    
    for(i=0; i < strlen(data); i++) {
        encText[i] ^= key[i % strlen(key)];
    }

    // Treat the string as raw bytes and pass in
    // length minus the null-terminator
    prettyEnc = rawBytesToPHex(encText, strlen(data));

    // Cleanup
    free(encText);
    
    return prettyEnc;
}

unsigned char* repeatKeyXORRaw(unsigned char *rawdata, unsigned char *key, int rawsize, int keylen) {
    unsigned char *encText = malloc(rawsize+1);
    encText[rawsize] = '\0';
    int i;

    memcpy(encText, rawdata, rawsize);

    for(i=0; i < rawsize; i++)
        encText[i] ^= key[i % keylen];

    return encText;
}

/**
 * Finds the differing bits between 2 equal length
 * strings
 */
int findHammingDist(char *arg1, char *arg2) {
    unsigned int len = strlen(arg1);
    unsigned int i, j;
    int ham = -1;
    int bit1, bit2;

    if(len != strlen(arg2))
        return ham;
    else
        ham = 0;
    
    for(i=0; i < len; i++) {
        for(j=0; j < 8; j++) {
            bit1 = (arg1[i] >> j) & 0x01;
            bit2 = (arg2[i] >> j) & 0x01;
            if(bit1 != bit2)
                ham++;
        }
    }

    return ham;
}

int findHammingDistRaw(char *arg1, char *arg2, int len1, int len2) {
    unsigned int i, j;
    int ham = -1;
    int res, bit;

    if(len1 != len2)
        return ham;
    else
        ham = 0;


    for(i=0; i < len1; i++) {
        res = arg1[i] ^ arg2[i];
        for(j=0; j < 8; j++) {
            bit = (res >> j) & 0x01;
            if(bit)
                ham++;
        }
    }

    return ham;
}

void testHammingDistance() {
    char *test1 = "this is a test";
    char *test2 = "wokka wokka!!!";
    char test3[3] = {0, 1, 2};
    char test4[3] = {4, 5, 6};
    int hamdist;
    int hamdist2;
    int ans = 37;

    hamdist = findHammingDist(test1, test2); 
    hamdist2 = findHammingDistRaw(test3, test4, 3, 3);

    printf("hamdist2: %d\n", hamdist2);

    if(hamdist < 0)
        printf("Strings are not same length!\n");
    else if(hamdist == ans)
        printf("Hamming correct!\n");
    else
        printf("Hamming incorrect! hamdist: %d\tanswer: %d\n", hamdist, ans);
}

void findBestKeySizes(char *rawdata, int rawsize, int *bestKeySizes, float *bestNorms) {
    int MAXKEYSIZE = 40;
    int KEYSIZE;
    int i;
    char firstKB[MAXKEYSIZE];
    char secondKB[MAXKEYSIZE];
    int hamDist;
    float normalized;


    for(KEYSIZE=2; KEYSIZE <= MAXKEYSIZE; KEYSIZE++) {
        if(rawsize < KEYSIZE*2)
            break;

        memcpy(firstKB, rawdata, KEYSIZE);
        memcpy(secondKB, rawdata+KEYSIZE, KEYSIZE);

        hamDist = findHammingDistRaw(firstKB, secondKB, KEYSIZE, KEYSIZE);

        if(hamDist < 0)
            break;

        normalized = (float)hamDist / (float)KEYSIZE; 

        for(i=0; i < 3; i++) {
            if(bestNorms[i] > normalized) {
                bestNorms[i] = normalized;
                bestKeySizes[i] = KEYSIZE;
                break;
            }
        }

    }
}

char* blockSolver(unsigned char *data, int datasize, int blocksize, unsigned char *key, int ecb) {
    int i, j;
    int itr = 0;
    unsigned int block_num = datasize/blocksize;
    unsigned char blocks[block_num][blocksize];
    unsigned char t_blocks[blocksize][block_num+1];
    unsigned char *plaintext;
    unsigned char masterkey[blocksize+1];
    AES_KEY dec_key;


    masterkey[blocksize] = '\0';

    // Cut rawdata into blocks of size blocksize (KEYSIZE)
    for(i=0; i < block_num; i++) {
        memcpy(blocks[i], data+itr, blocksize);
        itr += blocksize;
    }

    // Transpose blocks. First block contains first byte of above blocks,
    // second block contains second byte of above blocks, so on..
    for(i=0; i < blocksize; i++) {
        for(j=0; j < block_num; j++)
            t_blocks[i][j] = blocks[j][i];
        t_blocks[i][block_num] = '\0';
    }
    
    // Find the key within each transposed block
    for(i=0; i < blocksize; i++)
        masterkey[i] = findXORKey(t_blocks[i], block_num, ecb);

    if(ecb) {
        plaintext = malloc(datasize+1);
        plaintext[datasize] = '\0';
        AES_set_decrypt_key(masterkey, blocksize*8, &dec_key);

        i = 0;
        while(i < datasize) {
            AES_ecb_encrypt(data + i,
                plaintext + i, &dec_key, AES_DECRYPT); 
            i += blocksize+1;
        }
    }
    else
        plaintext = repeatKeyXORRaw(data, masterkey, datasize, blocksize);

    memcpy(key, masterkey, blocksize+1);

    return (char*)plaintext;
}

/**
 * Detect AES-ECB by detecting similar 16 byte blocks
 */
int blockECBDetector(char *data) {
    int datasize = strlen(data);
    int i, j;
    int duplicates = 0;
    char blocks[datasize/32][32+1];
    int dup_blocks[datasize/32];

    for(i=0; i < datasize/32; i++) {
        memcpy(blocks[i], data + (i*32), 32);
        blocks[i][32] = '\0';
        dup_blocks[i] = 0;
    }

    for(i=0; i < datasize/32; i++) {
        for(j=i+1; j < datasize/32; j++) {
            if(strcmp(blocks[i], blocks[j]) == 0 && dup_blocks[j] == 0) {
                dup_blocks[i] = 1;
                dup_blocks[j] = 1;
            }
        }
    }

    for(i=0; i < datasize/32; i++)
        duplicates += dup_blocks[i];

    return duplicates;
}


unsigned char* pkcs7Pad(char *str, char pad, unsigned int blockLen) {
    unsigned char *paddedStr = malloc(blockLen);
    int i = 0;

    memcpy(paddedStr, str, strlen(str));

    for(i=strlen(str); i < blockLen; i++) {
        paddedStr[i] = pad;
    }

    return paddedStr;
}
