#ifndef _AUXFUNCTIONS_H_
#define _AUXFUNCTIONS_H_

#include "hexTo64.h"
#include <openssl/aes.h>

char* fixedXOR(char *arg1, char *arg2);
int scoreLetterFrequency(char *str);
char* encryptDecrypt(char key, char *str);
unsigned char* encryptDecryptRaw(unsigned char key, unsigned char *str, int len);
char* encryptDecryptRawECB(unsigned char key, unsigned char *str, int len);
int readLine(FILE *fp, char *buffer, int bufsize, int *done);
int readFile(FILE *fp, char *buffer, int bufsize);
char findXORKey(unsigned char *data, int datasize, int ecb);
char* repeatKeyXOR(char *data, char *key);
unsigned char* repeatKeyXORRaw(unsigned char *rawdata, unsigned char *key, int rawsize, int keylen);
int findHammingDist(char *arg1, char *arg2);
int findHammingDistRaw(char *arg1, char *arg2, int len1, int len2);
void testHammingDistance();
void findBestKeySizes(char *rawdata, int rawsize, int *bestKeySizes, float *bestNorms);
char* blockSolver(unsigned char *data, int datasize, int blocksize, unsigned char *key, int ecb);
int blockECBDetector(char *data);

unsigned char* pkcs7Pad(char *str, char pad, unsigned int blockLen);

#endif // _AUXFUNCTIONS_H_
