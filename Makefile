CFLAGS = -Wall

main : main.o
	gcc $(CFLAGS) -o bin/main obj/main.o obj/set1Suite.o obj/set2Suite.o obj/auxFunctions.o obj/hexTo64.o -I/usr/local/ssl/include -L/usr/local/ssl/lib -lssl -lcrypto

main.o : set1Suite.o set2Suite.o
	gcc $(CFLAGS) -c src/main.c -o obj/main.o -I/usr/local/ssl/include

set1Suite.o : auxFunctions.o
	gcc $(CFLAGS) -c src/set1Suite.c -o obj/set1Suite.o -I/usr/local/ssl/include

set2Suite.o : auxFunctions.o
	gcc $(CFLAGS) -c src/set2Suite.c -o obj/set2Suite.o -I/usr/local/ssl/include

auxFunctions.o : hexTo64.o
	gcc $(CFLAGS) -c src/auxFunctions.c -o obj/auxFunctions.o -I/usr/local/ssl/include

hexTo64.o :  
	gcc $(CFLAGS) -c src/hexTo64.c -o obj/hexTo64.o

run :
	./bin/main

clean :
	rm obj/*.o bin/main
